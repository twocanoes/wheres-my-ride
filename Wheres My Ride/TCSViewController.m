//
//  TCSViewController.m
//  Bleu Auto
//
//  Created by Tim Perfitt on 4/12/14.
//  Copyright (c) 2014 Twocanoes Software, Inc. All rights reserved.
//

#import "TCSViewController.h"
#import "TCSBleuBeaconManager.h"
#import "TCSMapViewController.h"
#import <MapKit/MapKit.h>
extern NSString * const TCSDidEnterBleuRegionNotification;
extern NSString * const TCSDidExitBleuRegionNotification;
extern NSString * const TCSBleuRangingNotification;

@interface TCSViewController ()

@end

@implementation TCSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [TCSBleuBeaconManager sharedManager].entryText=@"Your car is nearby";
    
    [[NSNotificationCenter defaultCenter] addObserverForName:TCSDidEnterBleuRegionNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        
        [[TCSBleuBeaconManager sharedManager] beginRegionRanging];
    }];
    
    
    [[NSNotificationCenter defaultCenter] addObserverForName:TCSDidExitBleuRegionNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
                [[TCSBleuBeaconManager sharedManager] stopRegionRanging];
    }];
    
    
    [[NSNotificationCenter defaultCenter] addObserverForName:TCSBleuRangingNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        
        float distance=[[[[note userInfo] objectForKey:@"beacons"] firstObject] accuracy];
        int proximity=[[[[note userInfo] objectForKey:@"beacons"] firstObject] proximity];


        if (proximity!=CLProximityUnknown) [self updateBlipAtDistance:distance];
        else [[self.view viewWithTag:102] removeFromSuperview];
        
    }];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    TCSBleuBeaconManager *manager=[TCSBleuBeaconManager sharedManager];
        
    manager.major=2;
    manager.minor=1;
    manager.proximityUUID=@"6C5C3481-2666-45F8-9DE8-7F199FB4BA9A";
    [manager beginRegionRanging];
    

}
-(void)viewDidDisappear:(BOOL)animated{
    [[TCSBleuBeaconManager sharedManager] stopRegionRanging];
}
-(void)updateBlipAtDistance:(float)inDistance{
    
    float radius=50;

    float maxptdistance=self.view.bounds.size.width;
    
    float multiplier=maxptdistance/50.0;
    CGFloat x=self.view.center.x-radius/2;
    
    CGFloat y=self.view.center.y-radius/2;
    y=y+inDistance*multiplier;
    UIColor *currColor=[UIColor blueColor];
    
    
    CGRect circleRect = CGRectMake(x,y,
                                   radius,
                                   radius);
    
    [[self.view viewWithTag:102] removeFromSuperview];
    UIView *view = [[UIView alloc] initWithFrame:circleRect];
    view.tag=102;
    view.backgroundColor = currColor;
    view.layer.cornerRadius = radius/2;
    
    
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.duration = 1.0;
    
    scaleAnimation.repeatCount = HUGE_VAL;
    scaleAnimation.autoreverses = NO;
    scaleAnimation.fromValue = [NSNumber numberWithFloat:0.0];
    scaleAnimation.toValue = [NSNumber numberWithFloat:1.0];
    
    [view.layer addAnimation:scaleAnimation forKey:@"scale"];
    
    CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    
    
    fadeAnimation.duration = 1.0;
    
    fadeAnimation.repeatCount = HUGE_VAL;
    fadeAnimation.autoreverses = NO;
    fadeAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    fadeAnimation.toValue = [NSNumber numberWithFloat:0.0];
    
    [view.layer addAnimation:fadeAnimation forKey:@"opacity"];
    
    [self.view addSubview:view];
    
    
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
   CLLocationCoordinate2D coord=[[TCSBleuBeaconManager sharedManager] savedLocation];
    TCSMapViewController *mapViewController=[segue destinationViewController];
    
    mapViewController.carLocation=coord;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
