//
//  TCSBleuURLMapper.m
//  Bleu
//
//  Copyright (c) 2013 Twocanoes Software, Inc. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "TCSBleuBeaconManager.h"

NSString * const TCSDidEnterBleuRegionNotification = @"TCSDidEnterBleuRegionNotification";
NSString * const TCSDidExitBleuRegionNotification = @"TCSDidExitBleuRegionNotification";
NSString * const TCSBleuRangingNotification = @"TCSBleuRangingNotification";

@interface TCSBleuBeaconManager()
@property (nonatomic, strong) NSMutableDictionary *URLStore;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (strong, readwrite) NSString *defaultURL;
@property (strong,nonatomic) CLBeaconRegion *beaconRegion;

@end

@implementation TCSBleuBeaconManager
@synthesize URLStore = _URLStore;

+ (instancetype)sharedManager {
	static TCSBleuBeaconManager *_sharedManager;
	
	if (!_sharedManager) {
		_sharedManager = [[[self class] alloc] init];
		_sharedManager->_URLStore = [NSMutableDictionary dictionary];
			CLLocationManager *locationManager = [[CLLocationManager alloc] init];
			locationManager.delegate = _sharedManager;
			//These two settings sacrifice battery life for a smoother demo experience.
			[locationManager disallowDeferredLocationUpdates];
			locationManager.pausesLocationUpdatesAutomatically = NO;
			
			_sharedManager->_locationManager = locationManager;
	
		
	}
	return _sharedManager;
}




- (void)beginRegionRanging
{

    NSLog(@"-----------beginRegionMonitoring");
    NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:self.proximityUUID];
    _beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID major:self.major minor:self.minor identifier:@"com.twocanoes.region"];
    [self.locationManager startMonitoringForRegion:_beaconRegion];
	
}

-(void)stopRegionRanging{
    for (CLBeaconRegion *region in [self.locationManager rangedRegions] ){

        [self.locationManager stopRangingBeaconsInRegion:region];
    }
    NSLog(@"----------stop Ranging beacons");


}

#pragma mark - CLLocationManager Delegate

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    
	NSLog(@"Entered Region");
	UIApplicationState state = [[UIApplication sharedApplication] applicationState];
	switch (state) {
		case UIApplicationStateInactive:
		case UIApplicationStateActive:
			[[NSNotificationCenter defaultCenter] postNotificationName:TCSDidEnterBleuRegionNotification
																object:self
															  userInfo:@{@"region": region}];

			break;
		case UIApplicationStateBackground: {
			UILocalNotification *notification = [[UILocalNotification alloc] init];
			notification.alertBody = self.entryText;
            notification.soundName=UILocalNotificationDefaultSoundName;
			[[UIApplication sharedApplication] presentLocalNotificationNow:notification];
		}
		default:
			break;
	}

}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
  
    CLLocation *location=[locations lastObject];
    NSLog(@"Accuracy is %f",location.horizontalAccuracy);
    self.savedLocation=location.coordinate;

    NSLog(@"presenting notification");
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = @"Location Saved";
    notification.soundName=UILocalNotificationDefaultSoundName;
    [[TCSBleuBeaconManager sharedManager] startUpdatingLocation];
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    NSLog(@"presenting notification done");

    
    if (location.horizontalAccuracy<11.0) {
        
        UIAlertView *av=[[UIAlertView alloc] initWithTitle:@"Saved Location" message:@"The location of your car was saved!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [av show];
        [self.locationManager stopUpdatingLocation];
        
    }
}
- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error{
    NSLog(@"monitoriing failed with error %@",error.localizedDescription);
    
}

-(void)startUpdatingLocation{
    [self.locationManager startUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
	NSLog(@"Exited Region");
    if ([region isKindOfClass:[CLBeaconRegion class]]) {
        self.locationManager.desiredAccuracy=kCLLocationAccuracyBest;
        
    }

	UIApplicationState state = [[UIApplication sharedApplication] applicationState];
	switch (state) {
		case UIApplicationStateInactive:
		case UIApplicationStateActive:
			[[NSNotificationCenter defaultCenter] postNotificationName:TCSDidExitBleuRegionNotification
																object:self
															  userInfo:@{@"region": region}];

			break;
		case UIApplicationStateBackground:{
            [[TCSBleuBeaconManager sharedManager] startUpdatingLocation];

		}
		default:
			break;
	}
//	[manager startMonitoringForRegion:region];
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
	NSLog(@"Ranged %lu Beacons", (unsigned long)[beacons count]);
	if ([beacons count] > 0) {
		//Doesn't deal gracefully with multiple beacons in range
		CLBeacon *closestBeacon = beacons[0];
		NSNumber *index;
		switch (closestBeacon.proximity) {
			case CLProximityFar:
				index = @0;
				break;
			case CLProximityNear:
				NSLog(@"Near");
				index = @1;
				break;
			case CLProximityImmediate:
				NSLog(@"Immediate");
				index = @2;
				break;
			case CLProximityUnknown:
				NSLog(@"Unknown");
				index = @3;
				break;
		}
	}
    [[NSNotificationCenter defaultCenter] postNotificationName:TCSBleuRangingNotification object:self userInfo:@{ @"beacons": beacons}];

}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region{
	CLBeaconRegion *beaconRegion = (CLBeaconRegion *)region;
	switch (state) {
		case CLRegionStateInside:
			NSLog(@"Determined Inside State for Beacon %@", beaconRegion.identifier);
			if ([CLLocationManager isRangingAvailable]) {
				[self.locationManager startRangingBeaconsInRegion:beaconRegion];
			}
			break;
		case CLRegionStateOutside:
			NSLog(@"Determined Outside State for Beacon %@", beaconRegion.identifier);
			if ([CLLocationManager isRangingAvailable]) {
				[self.locationManager stopRangingBeaconsInRegion:beaconRegion];
			}
			break;
		case CLRegionStateUnknown:
			NSLog(@"Determined Unknown State for Beacon %@", beaconRegion.identifier);
			break;
	}
}


@end
