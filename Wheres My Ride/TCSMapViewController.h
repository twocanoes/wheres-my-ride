//
//  TCSMapViewController.h
//  Bleu Auto
//
//  Created by Tim Perfitt on 4/16/14.
//  Copyright (c) 2014 Twocanoes Software, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface TCSMapViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (assign,nonatomic) CLLocationCoordinate2D carLocation;
@end
